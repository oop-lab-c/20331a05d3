class parent {
    void temple() {
        System.out.println("Hi S V R");
    }
}

class simp extends parent {
    void people() {
        System.out.println("Good morning");
    }

    public static void main(String[] args) {
        simp obj = new simp();
        obj.temple();
        obj.people();
    }
}
