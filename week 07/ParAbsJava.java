abstract class  parent{
    abstract void dog();
    void cat(){
        System.out.println("cat has 4 legs");
    }
}
class child extends parent{
    void dog(){
         System.out.println("dog has 4 legs");
    }
    
}
class Main{
    public static void main(String[] args){
        child obj=new child();
        obj.dog();
        obj.cat();
    }
}