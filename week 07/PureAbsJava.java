interface animal{
    void lion();
    void deer();
}
class Main implements animal{
    public void lion(){
        System.out.println("lion roars");
    }
    public void deer(){
        System.out.println("deer jumps");
    }
    
    public static void main(String[] args){
        Main obj=new Main();
        obj.lion();
        obj.deer();
    }
}